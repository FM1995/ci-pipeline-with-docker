# CI Pipeline with Docker

### Pre-Requisites
Docker

Jenkins

Jenkins Plugin 'Maven'

Git

#### Project Outline

In this project we will devise a pipeline, where we will build the Java App using maven and its build tools, build the image using docker and then push to docker repository

![Image 1](https://gitlab.com/FM1995/ci-pipeline-with-docker/-/raw/main/Images/Image1.png)

Below are the git repositories used to complete this project

https://gitlab.com/FM1995/java-maven-app-2024/-/tree/main?ref_type=heads

https://gitlab.com/FM1995/java-maven-app-2024/-/tree/jenkins-jobs?ref_type=heads

#### Getting started

Lets begin using a basic Jenkinsfile

![Image 2](https://gitlab.com/FM1995/ci-pipeline-with-docker/-/raw/main/Images/Image2.png)

First step we will want to execute the build maven so that we can package it in to a JAR file
CMD’s to be installed in the Jenkinsfile
Under tools we can define the name of the maven installation so that we can make maven command available in the pipeline

•	Maven ‘maven’

Where we have defined the name of the maven installation in Dahsboard -> Manage Jenkins -> Tools -> Maven Installations -> Name ‘Maven’
In the “build jar” stage we can define the command to build the jar file

```
sh ’mvn package’
```

![Image 3](https://gitlab.com/FM1995/ci-pipeline-with-docker/-/raw/main/Images/Image3.png)

In the next step we will build the docker image and push it our private repository

As seen below we have the docker credentials available in Jenkins

![Image 4](https://gitlab.com/FM1995/ci-pipeline-with-docker/-/raw/main/Images/Image4.png)

So we will make it available in the Jenkinsfile
What we need to do in our next build stage is define ‘docker build’, ‘docker login’ and ‘docker push’ commands, so lets begin by creating a new empty stage

![Image 5](https://gitlab.com/FM1995/ci-pipeline-with-docker/-/raw/main/Images/Image5.png)


We can implement the below code to extract the previously mentioned docker credentials with ‘credentialsID’ and retrieve the username and password and assign to variable names such as ‘USER’ and ‘PASS’

```
withCredentials([usernamePassword(credentialsId: 'Docker credentials', passwordVariable: 'PASS', usernameVariable:'USER')])
```

With the credentials we are now able to execute the intended docker commands

First we will build the image with the below tag

```
sh 'docker build -t fuad95/demo-app:2024 .'
```

Then we will login to the private repository using the previously define variable ‘USER’ and ‘password-stdin’

```
sh 'docker login -u $USER --password-stdin'
```

And as a final command we will do docker push to the private repository

```
sh 'docker push fuad95/demo-app:2024'
```

And below we have a refined build image

![Image 6](https://gitlab.com/FM1995/ci-pipeline-with-docker/-/raw/main/Images/Image6.png)

Now ready to commit to gitlab with new branch ‘jenkins-jobs’

```
git checkout jenkins-jobs
git add .
git commit -m "Adding new branch"
git push origin jenkins-jobs
```

Can configure the multi-branch pipeline with the below criteria

![Image 7](https://gitlab.com/FM1995/ci-pipeline-with-docker/-/raw/main/Images/Image7.png)

Now ready to build and can see it now successful

![Image 8](https://gitlab.com/FM1995/ci-pipeline-with-docker/-/raw/main/Images/Image8.png)

Can see our image in docker hub

![Image 9](https://gitlab.com/FM1995/ci-pipeline-with-docker/-/raw/main/Images/Image9.png)

Next step would be to use a groovy script so that we have a cleaner jenkinsfile 
We can load the script.groovy script using load "script.groovy" in the init stage of our Jenkinsfile, can then define the functions buildJar(), buildImage(), and deployApp() in our Jenkinsfile

Below is the finalised Jenkinsfile

![Image 10](https://gitlab.com/FM1995/ci-pipeline-with-docker/-/raw/main/Images/Image10.png)

Below is the finalised groovy script

![Image 11](https://gitlab.com/FM1995/ci-pipeline-with-docker/-/raw/main/Images/Image11.png)

Now lets commit to the main branch

![Image 12](https://gitlab.com/FM1995/ci-pipeline-with-docker/-/raw/main/Images/Image12.png)

Can see build was successful on the main branch

![Image 13](https://gitlab.com/FM1995/ci-pipeline-with-docker/-/raw/main/Images/Image13.png)

#### References
https://gitlab.com/FM1995/java-maven-pipeline/-/blob/main/Jenkinsfile?ref_type=heads



